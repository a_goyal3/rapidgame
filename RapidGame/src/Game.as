package
{
	import events.NavigationEvent;
	
	import screens.GameOver;
	import screens.InGame;
	import screens.Welcome;
	
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class Game extends Sprite
	{
		private var screenWelcome:Welcome;
		
		private var screenInGame:InGame;
		
		private var gameOver:GameOver;
		
		private var chkinitbool:Boolean;
		
		public function Game()
		{
			super();
			chkinitbool= false;
			this.addEventListener(starling.events.Event.ADDED_TO_STAGE, onAddedToStage);
		}
		
		public function onAddedToStage(event:Event):void
		{
			// TODO Auto Generated method stub
			this.addEventListener(events.NavigationEvent.CHANGE_SCREEN,onChangeScreen);
			
			gameOver= new GameOver();
			gameOver.disposeTemp();
			this.addChild(gameOver);
			
			screenInGame= new InGame();
			screenInGame.disposeTemp();
			this.addChild(screenInGame);
			
			screenWelcome= new Welcome();
			this.addChild(screenWelcome);
			screenWelcome.initialize();
			
			
			
			
			
			
			
		}
		
		private function onChangeScreen(event:NavigationEvent):void
		{
			
			// TODO Auto Generated method stub
			switch (event.params.id)
			{case "play":
				screenWelcome.disposeTemp();
				if(chkinitbool == true)
				{
					chkinitbool= false;
				screenInGame.declare();	
				}
				screenInGame.initialize();
				break;
				
			case "game over":
				screenInGame.disposeTemp();
				screenInGame.deleteVector();
				InGame.bgSound.stop();
				
				
				gameOver.initialize();
				break;
			
			case "play again":
				
				gameOver.disposeTemp();
				chkinitbool= true;
				screenWelcome.initialize();
				break;
			}
		}
		}
	}
