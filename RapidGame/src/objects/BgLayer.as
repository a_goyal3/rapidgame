package objects
{
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class BgLayer extends Sprite
	{
		
		private var Image1:Image;
		private var Image2:Image;
		
		private var _layer:int;
		private var _parallax:Number;
		
		public function BgLayer(layer:int)
		{
			super();
			this._layer=layer;
			this.addEventListener(starling.events.Event.ADDED_TO_STAGE,onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{
			// TODO Auto Generated method stub
			this.removeEventListener(starling.events.Event.ADDED_TO_STAGE,onAddedToStage);
			
			if(_layer == 1)
			{
				Image1= new Image(Assets.getTexture("BgLayer"+_layer));
				Image2= new Image(Assets.getTexture("BgLayer"+_layer));
			}
			else if(_layer == 2)
			{
				Image1=new Image(Assets.getTexture("BgLayer"+_layer));
				Image2=new Image(Assets.getTexture("BgLayer"+_layer));
							
			}
			else
			{
				Image1=new Image(Assets.getTexture("BgLayer"+_layer));
				Image2=new Image(Assets.getTexture("BgLayer"+_layer));
				
			}
			
			Image1.x=0;
			Image1.y=stage.stageHeight - Image1.height;
			
			Image2.x=Image1.width;
			Image2.y=Image1.y;
			
			this.addChild(Image1);
			this.addChild(Image2);
			
			
			
		}
		
		public function get parallax():Number
		{
			return _parallax;
		}

		public function set parallax(value:Number):void
		{
			_parallax = value;
		}

	}
}