package objects
{
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class plane extends Sprite
	{
		
		private var myPlane:Image;
		public function plane()
		{
			super();
			this.addEventListener(starling.events.Event.ADDED_TO_STAGE,onAddedToStage);
			
		}
		
		private function onAddedToStage(event:Event):void
		{
			// TODO Auto Generated method stub
			this.removeEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
			createPlane();
		}
		
		private function createPlane():void
		{
			// TODO Auto Generated method stub
			myPlane=new Image(Assets.getTexture("InGamePlane"));
			myPlane.x = Math.ceil ( -myPlane.width/2);
			myPlane.y= Math.ceil (-myPlane.height/2);
			this.addChild(myPlane);
		}
	}
}