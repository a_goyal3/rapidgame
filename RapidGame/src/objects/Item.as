package objects
{
	import starling.display.Image;
	import starling.display.Sprite;
	
	public class Item extends Sprite
	{
		
		private var _minionType:int;
		private var itemImage:Image;
		
		
		
		public function Item(_minionType:int)
		{
			super();
			this.minionType=_minionType;
		}

		public function get minionType():int
		{
			return _minionType;
		}

		public function set minionType(value:int):void
		{
			_minionType = value;
			
			itemImage= new Image(Assets.getAtlas().getTexture("minion_"+ _minionType));
			itemImage.x = - itemImage.texture.width * 0.5;
			itemImage.y = - itemImage.texture.height *0.5;
			this.addChild(itemImage);
			
			
		}

	}
}