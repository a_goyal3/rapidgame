package screens
{
	import events.NavigationEvent;
	
	import flash.media.SoundChannel;
	
	import starling.animation.Transitions;
	import starling.animation.Tween;
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.utils.AssetManager;
	
	
	
	public class Welcome extends Sprite
	{
		private var gameName:Image;
		private var bg:Image;
		private var bg1:Image;// for background movement
		
		private var plane:Image;
		
		private var playBtn:Button;
		private var aboutBtn:Button;
		
		public function Welcome()
		{
			super();
			
			this.addEventListener(starling.events.Event.ADDED_TO_STAGE,onAddedToStage);
		}
		
		private function onAddedToStage(event:Event):void
		{
			// TODO Auto Generated method stub
			trace("welcome initialized");
			Assets.init();
			drawScreen();
		}
		
		private function drawScreen():void
		{
			// TODO Auto Generated method stub
			bg=new Image(Assets.getTexture("BgWelcome"));
			bg.x=0;
			bg.y=0;
			this.addChild(bg);
				
			bg1=new Image(Assets.getTexture("BgWelcome"));
			bg1.x=bg.width;
			bg1.y=0;
			this.addChild(bg1);
				
			plane=new Image(Assets.getTexture("WelcomePlane"));
				this.addChild(plane);
				plane.x = -plane.width;
				plane.y = 100;
			
			//title=new Image(Assets.getTexture("sky");
				//this.addChild(title);
				
		gameName = new Image(Assets.getTexture("GameName"));
		gameName.x = 500;	
		gameName.y = 50;
		this.addChild(gameName);
				
		playBtn=new Button(Assets.getTexture("WelcomePlayBtn"));
		playBtn.x=400;
		playBtn.y=200;	
		this.addChild(playBtn);
			
			
			
		aboutBtn=new Button(Assets.getTexture("WelcomeAboutBtn"));
		aboutBtn.x=300;
		aboutBtn.y=380;	
		this.addChild(aboutBtn);	
		
		this.addEventListener(Event.TRIGGERED,onMainMenuClick);	
			
			
		
		}
		
		private function onMainMenuClick(event:Event):void
		{
			// TODO Auto Generated method stub
			var btnClicked:Button = event.target as Button;
		if((btnClicked as Button) == playBtn)
		{
			this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id:"play"},true));
		}		
		}
		
		public function initialize():void
		{
			this.visible=true;
			
			
			plane.x = -plane.width;
			plane.y = 100;
			var tween:Tween = new Tween(plane, 2.0, Transitions.EASE_IN);
			tween.animate("x",plane.x + 380);
			//tween.fadeTo(0); // equivalent to 'animate("alpha", 0)'
			Starling.juggler.add(tween);
			
			
			this.addEventListener(Event.ENTER_FRAME, planeAnimation);
		}
		
		private function planeAnimation(event:Event):void
		{
			// TODO Auto Generated method stub
			var currentDate:Date=new Date();
			
			bg.x -= Math.ceil(0.02*20);
			bg1.x -= Math.ceil(0.02*20);
			
			if(bg.x < -bg.width)
			{	bg.x=0;
				bg1.x=bg.width;
			}
			
			plane.y = 100+(Math.cos(currentDate.getTime()*0.002)*30);
			
			playBtn.y = 200+(Math.cos(currentDate.getTime()*0.002)*10);
			
			aboutBtn.y = 380+(Math.cos(currentDate.getTime()*0.002)*10);
			
		}
		
		public function disposeTemp():void
		{
			// TODO Auto Generated method stub
			this.visible=false;
			if(this.hasEventListener(Event.ENTER_FRAME))
				this.removeEventListener(Event.ENTER_FRAME,planeAnimation);
		}
	}
}