package screens
{
	import events.NavigationEvent;
	
	import starling.display.Button;
	import starling.display.Image;
	import starling.display.Sprite;
	import starling.events.Event;
	
	public class GameOver extends Sprite
	{
		
		private var bg:Image;
		private var bg1:Image;
		
		
		private var playAgain:Button;
		private var gameOver:Image;
		private var gameMenu:Game;
		private var Ingamevar:InGame;
		
		
		
		
		public function GameOver()
		{
			super();
			
			
			this.addEventListener(starling.events.Event.ADDED_TO_STAGE,onAddedToStage);
			
		}
		
		private function onAddedToStage():void
		{
			// TODO Auto Generated method stub
			
			
			drawScreen();
		}
		
		private function drawScreen():void
		{
			// TODO Auto Generated method stub
			bg=new Image(Assets.getTexture("BgWelcome"));
			bg.x=0;
			bg.y=0;
			this.addChild(bg);
			
			bg1=new Image(Assets.getTexture("BgWelcome"));
			bg1.x=bg.width;
			bg1.y=0;
			this.addChild(bg1);
			
			
			playAgain=new Button(Assets.getTexture("playAgian"));
			playAgain.x=50;
			playAgain.y=200;	
			this.addChild(playAgain);
			
			
			
			gameOver=new Image(Assets.getTexture("GameOver"));
			gameOver.x=400;
			gameOver.y=200;	
			this.addChild(gameOver);	
			
			
			
			this.addEventListener(Event.TRIGGERED,onMainMenuClick);	
			
			
		
			
		}
		
		private function onMainMenuClick(event:Event):void
		{
			// TODO Auto Generated method stub
			var btnClicked:Button = event.target as Button;
			if((btnClicked as Button) == playAgain)
			{
				
				this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id:"play again"},true));
				
			}	
			
		}
		
		
		public function initialize():void
		{
			this.visible=true;
			this.addEventListener(Event.ENTER_FRAME, gameOverAnimation);

			
			
		}
		
		private function gameOverAnimation(event:Event):void
		{
			// TODO Auto Generated method stub
			var currentDate:Date=new Date();
			
			
			bg.x -= Math.ceil(0.02*20);
			bg1.x -= Math.ceil(0.02*20);
			
			if(bg.x < -bg.width)
			{	bg.x=0;
				bg1.x=bg.width;
			}
			
			playAgain.scaleX=playAgain.scaleY=0.8;
			gameOver.scaleX=gameOver.scaleY=0.8;
			
			playAgain.y = 200+(Math.cos(currentDate.getTime()*0.002)*10);
			
			gameOver.y = 200+(Math.cos(currentDate.getTime()*0.002)*10);
			
			
		}		
		
		public function disposeTemp():void
		{
			// TODO Auto Generated method stub
			this.visible=false;
			if(this.hasEventListener(Event.ENTER_FRAME))
				this.removeEventListener(Event.ENTER_FRAME,gameOverAnimation);
			
		}
		
	}
}