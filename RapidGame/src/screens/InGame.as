package screens
{
	import events.NavigationEvent;
	
	import flash.geom.Rectangle;
	import flash.media.SoundChannel;
	import flash.utils.getTimer;
	
	import objects.GameBackground;
	import objects.Item;
	import objects.Obstacle;
	import objects.Particle;
	import objects.plane;
	
	import starling.core.Starling;
	import starling.display.Button;
	import starling.display.Sprite;
	import starling.events.Event;
	import starling.events.Touch;
	import starling.events.TouchEvent;
	import starling.extensions.PDParticleSystem;
	import starling.text.TextField;
	import starling.textures.Texture;
	import starling.utils.HAlign;
	import starling.utils.VAlign;
	import starling.utils.deg2rad;
	
	public class InGame extends Sprite
	{
		
		private var startButton:Button;
		
		private var bg:GameBackground;
		private var Plane:plane;
		
		
		private var timePrevious:Number;
		private var timeCurrent:Number;
		private var timeElapsed:Number;
		
		private var gameState:String;
		
		private var playerSpeed:Number;
		private var hitObstacle:Number=0;
		private const MIN_SPEED:Number=650;
		
		private var scoreDistance:int;
		private var score:int;
		private var obstacleGapCount:int;
		private var lives:int;
		
		private var gameArea:Rectangle; 
		
		private var touch:Touch;
		private var touchX:Number;
		private var touchY:Number;
		
		
		private var obstaclesToAnimate:Vector.<Obstacle>;
		private var itemsToAnimate:Vector.<Item>;
		private var minParticlesToAnimate:Vector.<Particle>;
		
		private var distanceText:TextField;
		private var scoreText:TextField;
		private var livesText:TextField;
		private var intersectChecktoPlane:Rectangle;
		
		private var particle:PDParticleSystem;
		public static var crash:SoundChannel;
		public static var pickup:SoundChannel;
		public static var bgSound:SoundChannel;
		public function InGame()
		{
			super();
			
			this.addEventListener(starling.events.Event.ADDED_TO_STAGE,onAddedToStage);
		}
		private function onAddedToStage(event:Event):void
		{
			// TODO Auto Generated method stub
			this.removeEventListener(Event.ADDED_TO_STAGE,onAddedToStage);
			declare();
		}
		public function declare():void
		{
			drawPlane();
			
			scoreText= new TextField(300,100,"Score: 0","MyfontName",28,0x000000);
			scoreText.hAlign = HAlign.LEFT;
			scoreText.vAlign = VAlign.TOP;
			scoreText.x=50;
			scoreText.y=20;
			scoreText.height= scoreText.textBounds.height+ 10;
			
			this.addChild(scoreText);
			
			livesText= new TextField(300,100,"Lives: 5","MyfontName",28,0x000000);
			livesText.hAlign = HAlign.LEFT;
			livesText.vAlign = VAlign.TOP;
			livesText.x=250;
			livesText.y=20;
			livesText.height= livesText.textBounds.height+ 10;
			
			this.addChild(livesText);
			
			distanceText= new TextField(300,100,"Distance Covered: 0","MyfontName",28,0x000000);
			distanceText.hAlign = HAlign.LEFT;
			distanceText.vAlign = VAlign.TOP;
			distanceText.x=450;
			distanceText.y=20;
			distanceText.height= distanceText.textBounds.height+ 10;
			
			this.addChild(distanceText);
		}
		private function drawPlane():void
		{
			bg=new GameBackground();
			//bg.speed=10;
			this.addChild(bg);
			
			particle=new PDParticleSystem(XML (new Assets.particleXml),Texture.fromBitmap(new Assets.particleTexture));
			Starling.juggler.add(particle);
			
			
			particle.x = -100;
			particle.y = -100;
			
			particle.scaleX = 1.2;
			particle.scaleY = 1.2;
			this.addChild(particle);
			
			// TODO Auto Generated method stub
			Plane= new plane();
			Plane.x=stage.stageWidth/4;
			Plane.y=300;
			this.addChild(Plane);
				
			startButton=new Button(Assets.getTexture("startBtn"));
			startButton.x= stage.stageWidth*0.5 -startButton.width*0.5;
			startButton.y = stage.stageHeight*0.5- startButton.height*0.5;
			this.addChild(startButton);
			
			
			
			gameArea= new Rectangle(0,100, stage.stageWidth,stage.stageHeight -250);
			
			
			
		}
		
		public function disposeTemp():void
		{
			// TODO Auto Generated method stub
			this.visible=false;
			

		}
		
		public function initialize():void
		{	
			
			// TODO Auto Generated method stub
			this.visible=true;
			
			this.addEventListener(Event.ENTER_FRAME,checkElapsed);
	
			Plane.x= -stage.stageWidth;
			Plane.y= stage.stageHeight*0.5;
			
			gameState="idle";
			
			playerSpeed=0;
			hitObstacle=0;
			bg.speed=0;
			
			scoreDistance=0;
			score=0;
			lives=5;
			obstacleGapCount=0;
			
			obstaclesToAnimate = new Vector.<Obstacle>();
			itemsToAnimate =new Vector.<Item>();
			
			minParticlesToAnimate=new Vector.<Particle>();
			
			
			startButton.addEventListener(Event.TRIGGERED,onStartButtonClick);
		}
		
		private function onStartButtonClick(event:Event):void
		{
			// TODO Auto Generated method stub
			startButton.visible=false;
			startButton.removeEventListener(Event.TRIGGERED,onStartButtonClick);
			
			bgSound=Assets.backGroundSound.play(0,999);
			launchPlane();
		}
		private function launchPlane():void
		{
			trace( "In launch");
			particle.start();
			this.addEventListener(Event.ENTER_FRAME,onGameTick);
			this.addEventListener(TouchEvent.TOUCH,onTouch);
		}
		
		private function onTouch(event:TouchEvent):void
		{
			// TODO Auto Generated method stub
			touch= event.getTouch(stage);
			touchX=touch.globalX;
			touchY=touch.globalY;
		}
		
		private function onGameTick(event:Event):void
		{
			
			particle.x = Plane.x-60;
			particle.y = Plane.y;
			
			//trace(gameState);
			// TODO Auto Generated method stub
			switch(gameState)
			{
				case "idle":
					if(Plane.x < stage.stageWidth* 0.5*0.5)
					{
						Plane.x += ((stage.stageWidth * 0.5 * 0.5 + 10)- Plane.x) * 0.05;
						Plane.y = stage.stageHeight *0.5;
						
						playerSpeed += (MIN_SPEED - playerSpeed) * 0.05;
						bg.speed = playerSpeed * timeElapsed;
					}
					else
					{
						gameState = "flying";
					}
					break;
				
				case "flying":
					
					if(hitObstacle <=0)
					{
						Plane.y -= (Plane.y -touchY) *0.1;
					
						if(-(Plane.y - touchY) < 150 && -(Plane.y - touchY) > -150)
						{
							Plane.rotation= deg2rad( - (Plane.y - touchY) * 0.2);
						}
						
						
						
						if (Plane.y > gameArea.bottom - Plane.height * 0.5)
						{
							Plane.y = gameArea.bottom - Plane.height * 0.5;
							Plane.rotation = deg2rad(0);
						}
						if (Plane.y < gameArea.top + Plane.height * 0.5)
						{
							Plane.y = gameArea.top + Plane.height * 0.5;
							Plane.rotation = deg2rad(0);
						}
						
						
					}
					else
					{
						hitObstacle--;
						cameraShake();
					}
					
					
					
					
					playerSpeed -= (playerSpeed -MIN_SPEED) *0.01;
					
					bg.speed = playerSpeed * timeElapsed;
					
					
					scoreDistance += (playerSpeed * timeElapsed)*0.1;
			
					
					distanceText.text = "Distance Covered: "+ scoreDistance;
					initObstacle();		
					
					animateObstacles();
					
					createMinions();
					animateItmes();
					animateMinParticle();
					
					break;
				
			}
		}
		
		private function animateMinParticle():void
		{
			// TODO Auto Generated method stub
			for(var i:uint =0; i< minParticlesToAnimate.length; i++)
			{
				var minParticletoTrack:Particle = minParticlesToAnimate[i];
				
				if(minParticletoTrack)
				{
					minParticletoTrack.scaleX -= 0.03;
					minParticletoTrack.scaleY = minParticletoTrack.scaleX;
					
					minParticletoTrack.y -= minParticletoTrack.speedY;
					minParticletoTrack.speedY -= minParticletoTrack.speedY * 0.2;
					
					minParticletoTrack.x += minParticletoTrack.speedX;
					minParticletoTrack.speedX--;
					
					minParticletoTrack.rotation += deg2rad(minParticletoTrack.spin);
					minParticletoTrack.spin *= 1.1;
					
					if(minParticletoTrack.scaleY <=0.02)
					{
						minParticlesToAnimate.splice(i,1);
						this.removeChild(minParticletoTrack);
						minParticletoTrack=null;
					}
					
					
					
					
				}
			}
		}
		
		private function animateItmes():void
		{
			// TODO Auto Generated method stub
			var itemMinion:Item;
			
			for(var i:uint =0; i< itemsToAnimate.length; i++)
			{
				itemMinion = itemsToAnimate[i];
				itemMinion.x -= playerSpeed * timeElapsed;
				
				
				if(itemMinion.bounds.intersects(Plane.bounds))
					{
					
					createMinionParticles(itemMinion);
					pickup=Assets.MinionSound.play();
					
					score += 1;
					scoreText.text= "Score: "+score;
					
					itemsToAnimate.splice(i,1);
						this.removeChild(itemMinion);
					}
				
				if(itemMinion.x < -50)
				{
					itemsToAnimate.splice(i,1);
					this.removeChild(itemMinion);
					
				}				
			}
		}
		
		private function createMinionParticles(itemMinion:Item):void
		{
			// TODO Auto Generated method stub
			var count:int = 7;
			
			while(count>0)
			{
				count--;
				var minParticle:Particle = new Particle();
				this.addChild(minParticle);
				
				minParticle.x = itemMinion.x + Math.random() * 40 - 20;
				minParticle.y = itemMinion.y + Math.random() * 40;
				
				minParticle.speedX = Math.random() *2 + 1;
				minParticle.speedY = Math.random() * 5 ;
				minParticle.spin   = Math.random() * 15;
				
				minParticle.scaleX = minParticle.scaleY = Math.random() * 0.3 +0.3;
			
				minParticlesToAnimate.push(minParticle);
			}
		}
		
		private function createMinions():void
		{
			// TODO Auto Generated method stub
			if(Math.random() > 0.95)
			{
				var ItemToTrack:Item = new Item(Math.ceil ( Math.random() * 7));
				ItemToTrack.x= stage.stageWidth + 50;
				ItemToTrack.y = int(Math.random() * (gameArea.bottom - gameArea.top)+ gameArea.top);
				this.addChild(ItemToTrack);
				
				itemsToAnimate.push(ItemToTrack);
			}
		}
		
		private function cameraShake():void
		{
			// TODO Auto Generated method stub
			if(hitObstacle >0 )
			{
				this.x = Math.random() * hitObstacle;
				this.y = Math.random() * hitObstacle;
				
			}
			else if (x!=0)
			{
				this.x=0;
				this.y=0;
				
			}
		}
		
		private function animateObstacles():void
		{
			// TODO Auto Generated method stub
			var obstacleToTrack:Obstacle;
			
			for(var i:uint=0 ; i< obstaclesToAnimate.length ; i++)
			{
				obstacleToTrack =obstaclesToAnimate[i];
	
				
				if(obstacleToTrack.alreadyHit == false && obstacleToTrack.bounds.contains( Plane.bounds.left+Plane.width/2,Plane.bounds.top+Plane.height/2+20))
				{
					crash=Assets.ImpactSound.play();
					lives--;
					livesText.text = "Lives: "+ lives;
					
					if(lives == 0)
 					{
						gameState = "over"
						this.dispatchEvent(new NavigationEvent(NavigationEvent.CHANGE_SCREEN, {id:"game over"},true));
		
					}




					if(score <= 10)
					{
						score=0;
						scoreText.text= "Score: "+score;
						scoreDistance =0;						
						distanceText.text = "Distance Covered: "+ scoreDistance;
					}
					else
					{
						score -= 10;
						scoreText.text= "Score: "+score;
					}
					obstacleToTrack.alreadyHit =true;
					obstacleToTrack.rotation = deg2rad(70);
					hitObstacle =30;
					playerSpeed *= 0.5;
				}
				
				
				if(obstacleToTrack.distance>0)
				{
					obstacleToTrack.distance -= playerSpeed * timeElapsed;
					
				}
				else
				{
					if(obstacleToTrack.watchOut)
					{
						obstacleToTrack.watchOut=false;
						
					}
					obstacleToTrack.x -= (playerSpeed + obstacleToTrack.speed) * timeElapsed;
					
				}
				
				if(obstacleToTrack.x < -obstacleToTrack.width || gameState == "over")
				{
					obstaclesToAnimate.splice(i,1);
					this.removeChild(obstacleToTrack);
				}
				
				
			}
			
			
		}
		
		private function initObstacle():void
		{
			// TODO Auto Generated method stub
			if(obstacleGapCount < 2400)
			{
				obstacleGapCount += playerSpeed * timeElapsed;
				
			}
			else if (obstacleGapCount !=0)
			{
				obstacleGapCount = 0;
				createObstacle(Math.ceil ( Math.random()*4),Math.random()*1000+1000);
			}
		}
		
		private function createObstacle(type:Number, distance:Number):void
		{
			// TODO Auto Generated method stub
			var obstacle:Obstacle = new Obstacle(type, distance, true, 300);
			obstacle.x = stage.stageWidth;
			this.addChild(obstacle);
			
			
			if(type <=2 || type == 4)
			{
				if(Math.random()> 0.5 )
				{
					obstacle.y=gameArea.top;
					obstacle.position ="top";
					
				}
				else
				{
					obstacle.y= gameArea.bottom- obstacle.height;
					obstacle.position= "bottom";
				}
			}
			else
			{
				obstacle.y= int (Math.random() * (gameArea.bottom - obstacle.height - gameArea.top)) + gameArea.top;
				obstacle.position="middle";
			}
			
			obstaclesToAnimate.push(obstacle);
			
		}
		private function checkElapsed(event:Event):void
		{
			// TODO Auto Generated method stub
			timePrevious=timeCurrent;
			timeCurrent= getTimer();
			timeElapsed =(timeCurrent - timePrevious) *0.001;
			
		}

	public function deleteVector():void
     {
		trace("deleting");
		this.removeEventListener(Event.ENTER_FRAME,onGameTick);
		this.removeEventListener(TouchEvent.TOUCH,onTouch);
		
		particle.dispose();
		Plane.dispose();
	
		if(itemsToAnimate.length > 0)
		{
		var itemMinion:Item;
		
			for(var i:uint =0; i< itemsToAnimate.length; i++)
			{
				itemMinion = itemsToAnimate[i];
				itemsToAnimate.splice(i,1);
				this.removeChild(itemMinion);
			
			}
		}
		
		obstaclesToAnimate.length=0;
		itemsToAnimate.length=0;
		minParticlesToAnimate.length=0;

     }
	
	}
}