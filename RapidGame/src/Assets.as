package
{
	import flash.display.Bitmap;
	import flash.media.Sound;
	import flash.media.SoundChannel;
	import flash.media.SoundTransform;
	import flash.utils.Dictionary;
	
	import flashx.textLayout.formats.BackgroundColor;
	
	import starling.textures.Texture;
	import starling.textures.TextureAtlas;

	public class Assets
	{
		[Embed(source="../assets1/sky3.png")]
		public static const BgWelcome:Class;
		
		[Embed(source="../assets1/sky3.png")]
		public static const BgLayer1:Class;
		
		[Embed(source="../assets1/hills.png")]
		public static const BgLayer2:Class;
		
		[Embed(source="../assets1/city.png")]
		public static const BgLayer3:Class;
		
		[Embed(source="../assets1/Plane.png")]
		public static const WelcomePlane:Class;
		
		[Embed(source="../assets1/GameName.png")]
		public static const GameName:Class;
		
		[Embed(source="../assets1/PlaneInGame.png")]
		public static const InGamePlane:Class;
		
		[Embed(source="../assets1/play_btn.png")]
		public static const WelcomePlayBtn:Class;
		
		[Embed(source="../assets1/about_btn.png")]
		public static const WelcomeAboutBtn:Class;
		
		[Embed(source="../assets1/start_btn.png")]
		public static const startBtn:Class;
		
		[Embed(source="../assets1/PLAYAGIAN.png")]
		public static const playAgian:Class;
		
		[Embed(source="../assets1/GAME_OVER.png")]
		public static const GameOver:Class;
		
		private static var gameTextures:Dictionary = new Dictionary();
		private static var gameTextureAtlas:TextureAtlas;
		
		[Embed(source="../assets1/myPlaneSpriteSheet.png")]
		public static const AtlasTextureGame:Class;
		
		[Embed(source="../assets1/myPlaneSpriteSheet.xml", mimeType="application/octet-stream")]
		public static const AtlasXmlGame:Class;
		
		
		[Embed(source="../assets1/ARBERKLEY.TTF", fontFamily="MyFontName",embedAsCFF="flase" )]
		public static var MyFont:Class;
		
		
		[Embed(source="../assets1/drip.mp3")]
		private static var Minion_Sound:Class;
		public static var MinionSound:Sound;
		
		[Embed(source="../assets1/GameBackGround.mp3")]
		private static var backGround:Class;
		public static var backGroundSound:Sound;
		
		[Embed(source="../assets1/crash.mp3")]
		private static var Impact:Class;
		public static var ImpactSound:Sound;
		
		[Embed(source="../assets1/particle.pex", mimeType="application/octet-stream")]
		public static var particleXml:Class;
		
		[Embed(source="../assets1/texture.png")]
		public static var particleTexture:Class;
		
		
		
		
		
		public static function init():void
		{
			trace("Sound bar");
			MinionSound = new Minion_Sound();
			MinionSound.play(0,0, new SoundTransform(0));
			
			backGroundSound = new backGround();
			backGroundSound.play(0,0,new SoundTransform(0));
			
			ImpactSound = new Impact();
			ImpactSound.play(0,0,new SoundTransform(0));
			
			
		}
		
		
		
		public static function getAtlas():TextureAtlas
		{
			if(gameTextureAtlas == null)
			{
				var texture:Texture= getTexture("AtlasTextureGame");
				var xml:XML =XML(new AtlasXmlGame());
				
				gameTextureAtlas = new TextureAtlas(texture,xml);
			}
			return gameTextureAtlas;
		}
		
		public static function getTexture(name:String):Texture
		{
		if(gameTextures[name] == undefined)
		{
			var bitmap:Bitmap = new Assets[name]();
			gameTextures[name]=Texture.fromBitmap(bitmap);
			
		}
		return gameTextures[name];
		}
		
		}
	}
