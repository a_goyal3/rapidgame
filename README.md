Group Members-
	Adit Goyal
	Aditya Mattoo
	Nishant Sthalekar

How to run-
	Just Double -click the RapidGame.swf file in bin-release folder.
	
	If Flash Player is not installed then-
	
	There is a FlashPlayer 17 .executable provided in the tools. 
	Just open it and drag rapidGame.swf file from the bin-Release folder upon it. 

Game Environment-

	Made in Flash Builder 4.6 with Starling libraries, Hi-res-stats library, Starling Particle extension, Texture Packer for sprite sheet etc.


Game Idea-

	There is a plane to rescue all the minions in the air and Player has to dodge through the obstacles in the game.
	There is a welcome screen where logo plane model and buttons are present.
	Play button is used to move to in-game screen and Upon pressing the start button the game starts
	The player has 5 lives and each minion increases 1 score and each crash costs 10 scores and one life.
	The distance is also calculated which can be put in stats for all players. 
	Implementation of score table and databases are not maintained


Physics-

	The Plane is having movement on mouse control and also has some rotation for ease in feature.
	There is a box on box collision detection for minions and obstacles.
	The background layers are 3 png's which are set in parallax movement.
	
Animation-

	There is warning sign which is a sprite animation of 4 png files and 
	Also, there is a helicopter obstacle which is also animated with 3 pngs.
	
	There is generation of particle effects on minions collisions and there is 
	implementation of a particle from starling particle extension by downloading "pex" file from onebyonedesign.com
	
Sound-

	There is a background sound which plays and stops according to game start and game over.
	There is 2 sound effect of minion pick ups and Obstacle crashes to make the game some lively.